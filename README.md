# k6 tool - grafana dashboard - influxdb DB
the **k6 tool**  is a  open-source load testing tool.


### Starting 🚀
These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.

### Prerequisites 📋
- [Git Bash](https://www.npmjs.com/get-npm).
- [Node](https://nodejs.org/en/download/).
- [npm](https://www.npmjs.com/get-npm).
- [docker](https://docs.docker.com/get-docker/).
- [docker-compose](https://docs.docker.com/compose/install/).

####  What things do you need to configure the software.

- None


##  Create Docker containers

Open a Command Prompt or Terminal in the Folder Project, and run 
docker-compose up -d influxdb grafana k6
 
 the result
Creating load_testing_tool_influxdb_1 ... done
Creating load_testing_tool_k6_1       ... done
Creating load_testing_tool_grafana_1  ... done


####  we will have the following tools available and already integrated

- influxdb
  http://influxdb:8086

- Grafana Dashboard
  http://localhost:3003/
  http://localhost:3003/d/ReuNR5Aik/k6-load-testing-results

- k6 load testing tool
  volume mounted from local to container -> k6_framework:/home/k6

 #### the structure of the project
- k6_framework/resources
  here we declare the general and specific resources of our services

- k6_framework/src/service
  the execution entry of each of our services

####  An example
- get activities services
k6_framework/resources/activities_endpoint

- Here we declare the common body, common header, and the end of our service url
k6_framework/src/services/activities/get_classes_activities/
  the execution entry point

- how to run the test
inside container

docker exec -it container_K6_ID sh

- move to the location of your service entry point
cd src/services/activities/get_classes_activities/

- run the following command
-  
k6 run -e TYPE_TEST=stress_test index.js

you can use 3 types of TYPE_TEST [smoke_test,load_test,stress_test]

When running the tests using docker the results can be reported to datadog. The docker compose file has 2 services,
one to run the test with k6 and the datadog agent which will receive and transmit the results.

To execute them you must set the following environment variables

| Variable         | Purpose                                                                                  |
| ---------------- | ---------------------------------------------------------------------------------------- |
| DD_API_KEY       | Datadog api key                                                                          |
| DD_ENV           | Datadog environment                                                                      |
| DD_SERVICE_NAME  | Datadog service name                                                                     |
| TEST_SCENARIO    | K6 test scenario you want to execute                                                     |
| TEST_PATH        | Relative path to the test you want to execute                                            |


tools documentation 
https://k6.io/docs/es
https://github.com/k6io/postman-to-k6
docker-compose run k6 run /services/service/index.js
