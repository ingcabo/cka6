
// ropo el segmento c_secretUser es un parametro fijate en los reportes de newman ejemplos y en el ejemplo de postman, y recorda el formato fecha
const url = 'customer-identity-access-and-consent/v1/customers';
const headers = {
    'X-Channel': '{{c_channel}}',
    'X-Country': '{{c_country}}',
    'X-Commerce': '{{c_commerce}}',
    'X-Date-Time': '{{c_date_iso}}',
    'X-Session-Id': "",
    'X-Transaction-Type': '{{c_transaction_type}}',
    'X-Operator': '123',
    'X-Trace-Id': '123',
    'X-Forwarded-For': '10.154.155.218',
    'Content-Type': 'application/json',
    'accept': 'application/json'
}

const body = {
    "authentication": {
        "user": {
            "username": "{{c_username}}",
            "usernameType": "1"
        },
        "password": "{{c_password_hash}}"
    }
    
    
}


const parametrization_test = {
    "smoke_test": {
        vus: 1,  // 1 user looping for 1 minute
        duration: '10s',
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',   // variable de entorno
            layer: 'pb',   // variable de entorno
            env: 'dev',    // variable de entorno
            service: 'customer-identity-auth',
            type_test: 'smoke_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-identidad-validar-password_2.json',
    },
    "load_test": {
        stages: [
            { duration: "5m", target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
            { duration: "10m", target: 100 }, // stay at 100 users for 10 minutes
            { duration: "5m", target: 0 }, // ramp-down to 0 users
        ],
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
            'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',     // variable de entorno
            layer: 'pb',     // variable de entorno
            env: 'dev',      // variable de entorno
            service: 'customer-identity-auth',
            type_test: 'load_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-identidad-validar-password.json',
    },
    "stress_test": {
        stages: [
            { duration: '2m', target: 100 }, // below normal load
            { duration: '5m', target: 100 },
            { duration: '2m', target: 200 }, // normal load
            { duration: '5m', target: 200 },
            { duration: '2m', target: 300 }, // around the breaking point
            { duration: '5m', target: 300 },
            { duration: '2m', target: 400 }, // beyond the breaking point
            { duration: '5m', target: 400 },
            { duration: '10m', target: 0 }, // scale down. Recovery stage.
        ],
        tags: {
            stack: 'bb',
            layer: 'pb',
            env: 'dev',
            service: 'customer-identity-auth',
            type_test: 'stress_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-identidad-validar-password.json',
    }
}

module.exports = {
    url,
    headers,
    body,
    parametrization_test
}

