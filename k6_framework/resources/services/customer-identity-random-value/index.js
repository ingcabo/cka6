
const url = 'customer-identity-access-and-consent/v1/random-value';

const headers = {
    'X-Country': 'PE',
    'X-Commerce': 'BANCO',
    'X-Channel': 'Web',
    'X-Date-Time': 'XXXXXXX',
    'Authorization': 'Bearer blablabla',
    'Content-Type': 'application/json',
    'accept': 'application/json'
}

const body ={
    user:
    {
        username: "{{c_username}}",
        usernameType: "1"
    }
}


const parametrization_test = {
    "smoke_test": {
        vus: 1,  // 1 user looping for 1 minute
        duration: '5s',
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',   // variable de entorno
            layer: 'pb',   // variable de entorno
            env: 'dev',    // variable de entorno
            service: 'customer-identity-random-value',
            type_test: 'smoke_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cadena-aleatoria-obtener.json',
    },
    "smoke_test_ok": {
        vus: 45,  // 1 user looping for 1 minute
        duration: '5s',
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',   // variable de entorno
            layer: 'pb',   // variable de entorno
            env: 'dev',    // variable de entorno
            service: 'customer-identity-random-value',
            type_test: 'smoke_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cadena-aleatoria-obtener_2.json',
    },
    "load_test": {
        stages: [
            { duration: "5m", target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
            { duration: "10m", target: 100 }, // stay at 100 users for 10 minutes
            { duration: "5m", target: 0 }, // ramp-down to 0 users
        ],
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
            'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',     // variable de entorno
            layer: 'pb',     // variable de entorno
            env: 'dev',      // variable de entorno
            service: 'customer-identity-random-value',
            type_test: 'load_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cadena-aleatoria-obtener.json',
    },
    "stress_test": {
        stages: [
            { duration: '2m', target: 100 }, // below normal load
            { duration: '5m', target: 100 },
            { duration: '2m', target: 200 }, // normal load
            { duration: '5m', target: 200 },
            { duration: '2m', target: 300 }, // around the breaking point
            { duration: '5m', target: 300 },
            { duration: '2m', target: 400 }, // beyond the breaking point
            { duration: '5m', target: 400 },
            { duration: '10m', target: 0 }, // scale down. Recovery stage.
        ],
        tags: {
            stack: 'bb',
            layer: 'pb',
            env: 'dev',
            service: 'customer-identity-random-value',
            type_test: 'stress_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cadena-aleatoria-obtener.json',
    }
}

module.exports = {
    url,
    headers,
    body,
    parametrization_test
}

