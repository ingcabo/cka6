
const url = 'customer-account-and-preferences/v1/customers';
const headers = {
    'X-Channel': '{{c_channel}}',
    'X-Country': '{{c_country}}',
    'X-Commerce': '{{c_commerce}}',
    'X-Customer-Id': '{{c_username}}',
    'X-Document-Id': '{{c_username_type}}',
    'Content-Type': 'application/json',
    'accept': 'application/json'
}

const body = {
    
    
}


const parametrization_test = {
    "smoke_test": {
        vus: 1,  // 1 user looping for 1 minute
        duration: '10s',
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',   // variable de entorno
            layer: 'pb',   // variable de entorno
            env: 'dev',    // variable de entorno
            service: 'customer-account-general-information',
            type_test: 'smoke_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cliente-informacion-general-obtener_2.json',
    },
    "load_test": {
        stages: [
            { duration: "5m", target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
            { duration: "10m", target: 100 }, // stay at 100 users for 10 minutes
            { duration: "5m", target: 0 }, // ramp-down to 0 users
        ],
        thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
            'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: {
            stack: 'bb',     // variable de entorno
            layer: 'pb',     // variable de entorno
            env: 'dev',      // variable de entorno
            service: 'customer-account-general-information',
            type_test: 'load_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cliente-informacion-general-obtener.json',
    },
    "stress_test": {
        stages: [
            { duration: '2m', target: 100 }, // below normal load
            { duration: '5m', target: 100 },
            { duration: '2m', target: 200 }, // normal load
            { duration: '5m', target: 200 },
            { duration: '2m', target: 300 }, // around the breaking point
            { duration: '5m', target: 300 },
            { duration: '2m', target: 400 }, // beyond the breaking point
            { duration: '5m', target: 400 },
            { duration: '10m', target: 0 }, // scale down. Recovery stage.
        ],
        tags: {
            stack: 'bb',
            layer: 'pb',
            env: 'dev',
            service: 'customer-account-general-information',
            type_test: 'stress_test'
        },
        filename: '../../../data/'+ __ENV.COUNTRY_CODE +'/test-case-cliente-informacion-general-obtener.json',
    }
}

module.exports = {
    url,
    headers,
    body,
    parametrization_test
}

