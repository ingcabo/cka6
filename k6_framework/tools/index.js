import encoding from 'k6/encoding';
import crypto from 'k6/crypto';

function entryLog(data, expectedStatusCode='200'){
    console.log(`Http -  expectedStatusCode ####`,expectedStatusCode);
    console.log(`Http -  Status ####`,data.status);
    console.log(`Http -  Body ####`,data.body);
}

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function expire_date(expires_in,idname) {
    let now = new Date();
    const moment = require("moment");
    let mydate = Number(moment(now).format("YYYYMMDDHHmmss"));

    if (mydate >= expires_in) {
      console.log(`Expired token_Bearer_${idname}`);
    } else {
      let token = postman.getEnvironmentVariable(`tokenBearer_${idname}`);
      pm.environment.set("tokenBearer", token);
      console.log(`valid token_Bearer_${idname}`);
    }
  }

  function when_expire_token(expires_in){
    const moment= require('moment');
    let now = new Date();
    let expiryDate =new Date(now.getTime() + expires_in *999.11);
    expiryDate = Number(moment(expiryDate).format("YYYYMMDDHHmmss"));
    return expiryDate;
}

function randomString(length, chars) {
  var mask = '';
  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';
  if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  var result = '';
  for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
  return result;
}

function metramd (myarray){
  let payment = Math.floor(Math.random() * myarray.length);
  return payment;
  
}

function formatDate (pminutes=0, _day,_month){
  let date = new Date();
  date = new Date(date.setTime(date.getTime() + (pminutes * 60 * 1000)));
  const hour = date.getHours().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const day = _day || date.getDate();
  const month =  _month || (date.getMonth() + 1);
  const year = date.getFullYear();
  const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`;
  return formattedDate;
};

function getFormatedDate(format){
  let date = new Date();
  const hour = date.getHours().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const day = date.getDate();
  const month =  date.getMonth() + 1;
  const year = date.getFullYear();
  const formattedDate = getFormat(format, year, month, day, hour, minutes, seconds);

  return formattedDate;
}

function getFormat(format, year, month, day, hours, minutes, seconds) {

  let formattedDate = '';
  switch (format) {
      case "YYYY/MM/DD 00:00:00":
          formattedDate = `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`;
      break;
      case "DD/MM/YYYY 00:00:00":
          formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
      break;
      case "YYYY/MM/DD":
          formattedDate = `${year}/${month}/${day}`;
      break;
      case "DD/MM/YYYY":
          formattedDate = `${day}/${month}/${year}`;
      break;
      case "MM/DD/YYYY 00:00:00":
          formattedDate = `${month}/${day}/${year} ${hours}:${minutes}:${seconds}`;
      break;
      case "MM/DD/YYYY":
          formattedDate = `${month}/${day}/${year}`;
      break;
      case "MM-DD-YYYY 00:00:00":
          formattedDate = `${month}-${day}-${year} ${hours}:${minutes}:${seconds}`;
      break;
      case "MM-DD-YYYY":
          formattedDate = `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
      break;
      case "YYYY-MM-DD 00:00:00":
          formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
      break;
      case "YYYY-MM-DDT00:00:00Z":
          formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}Z`;
      break;
      case "DD-MM-YYYY 00:00:00":
          formattedDate = `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
      break;
      case "YYYY-MM-DD":
          formattedDate = `${year}-${month}-${day}`;
      break;
      case "DD-MM-YYYY":
          formattedDate = `${day}-${month}-${year}`;
      break;
  
      default:
          formattedDate = `${year}/${month}/${day}`;
          break;        
  }
  return formattedDate;
}

function getSecretUser(secret){
    return secret;
}

function millisToSeconds(time){
  return time; ///60000;
}

function writeFile(fileDire,newObj) {
  const fs = require('fs');
  
  let content = JSON.stringify(newObj); 
  console.log("content",content)
  return fs.writeFile(`${fileDire}`, content,'utf8', function (err) {
      if (err) throw err;
  });  
}


function createRandomDeviceName(prefix){
  let ts = Math.round((new Date()).getTime() / 1000);
  let serialNo = `${prefix}${ts}`
  return serialNo;
}


function envFileImport(){
    switch(__ENV.TYPE_TEST) {
        case 'pe': {
            return `env_pe`;
        }
        case 'cl': {
            return `env_cl`;
        }
        default: {
            return `env_co`;
        }
    }
}

function covertToBase64(consumerKey,consumerSecret,separator=":"){
    var base64 = encoding.b64encode(consumerKey+separator+consumerSecret);
    return base64;
}

function stringToSha256(str){
    console.log("str "+str)
    var sha256 = crypto.sha256(unescape(encodeURIComponent(str)), 'hex');

    return sha256;
}



module.exports = {
    entryLog,
    create_UUID,
    randomString,
    metramd,
    formatDate,
    millisToSeconds,
    writeFile,
    createRandomDeviceName,
    envFileImport,
    covertToBase64,
    getFormatedDate,
    getSecretUser,
    stringToSha256
}