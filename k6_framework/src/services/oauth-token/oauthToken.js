import http from 'k6/http';
import {baseUrlService, CONSUMER_KEY, CONSUMER_SECRET} from '../../../resources/services/general.js'
import {body,headers,url}  from '../../../resources/services/oauth-token/index.js'
import * as tools from "../../../tools/index.js";


export let execute = () => {

    const c_authorization_token = tools.covertToBase64(CONSUMER_KEY,CONSUMER_SECRET);

    headers['x-api-key']= CONSUMER_KEY;
    headers['Authorization']= `Basic ${c_authorization_token}`;

    let response = http.post(`${baseUrlService}/${url}`, body, {headers});

    return response;
}