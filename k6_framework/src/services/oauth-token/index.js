import { check, group, sleep, fail } from 'k6';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import * as tools from '../../../tools/index.js';
import * as oauthToken_params from '../../../resources/services/oauth-token/index.js'
import * as oauthToken from './oauthToken.js'
import {CONSUMER_KEY, CONSUMER_SECRET} from "../../../resources/services/general.js";


export let options = oauthToken_params.parametrization_test[__ENV.TYPE_TEST]

export function setup() {

  const c_authorization_token = tools.covertToBase64(CONSUMER_KEY,CONSUMER_SECRET);

  return{
    x_api_key:CONSUMER_KEY,
    c_authorization_token
  }
}


export default (data) => {

  let res = oauthToken.execute(data)
  if (res.status != 200){
    tools.entryLog(res);
  }


  check(res, {
    'response code was 200': (res) => res.status == 200,
    'access_token contain data': (res) => JSON.parse(res.body).access_token.length > 20,
    'token_type  equal to  Bearer': (res) => JSON.parse(res.body).token_type = "Bearer"
  });


}

export function handleSummary(data) {
  return {
    "oauthToken.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}


