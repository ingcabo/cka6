//report
import {check, sleep} from 'k6';
import {Rate} from 'k6/metrics'
import {htmlReport} from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import {textSummary} from "https://jslib.k6.io/k6-summary/0.0.1/index.js";
import * as oauthToken from '../oauth-token/oauthToken.js'
import * as cIdentityRValue_params from '../../../resources/services/customer-identity-random-value/index.js'
import * as cIdentityRValue from './customer-identity-random-value.js'
import * as tools from '../../../tools/index.js';


export let options = cIdentityRValue_params.parametrization_test[__ENV.TYPE_TEST]
const userData = JSON.parse(open(options.filename));

export let errorRateHttpStatus = new Rate('errorRateHttpStatus');
export let errorRateBodyResponse = new Rate('errorRateBodyResponse');

export function setup() {

    let {body} = oauthToken.execute();
    let {access_token} = JSON.parse(body);

    userData.forEach(element => {
        element['access_token'] = access_token;
    });

    return userData;
}


export default (userData) => {
    //este count itera uno por uno los valores del json
    let count = tools.metramd(userData);
    let data = userData[count];

    let {c_expectedStatusCode} = data;
    let res = cIdentityRValue.execute(data);

    if (res.status != c_expectedStatusCode) {
        tools.entryLog(res,c_expectedStatusCode);
    }


    if (res.status == 200) {
        const check_200 = check(res, {
            'body contain data id': (res) => JSON.parse(res.body).id.length > 20,
            'body contain data salt': (res) => JSON.parse(res.body).salt.length > 20
        })
    }


    const check_expectedStatusCode = check(res, {

        'response code was expected': (res) => res.status == c_expectedStatusCode,
    })

    //errorRateHttpStatus.add(!check_200);
    errorRateBodyResponse.add(!check_expectedStatusCode);


   // sleep(1)
}

export function handleSummary(data) {
    const date =tools.getFormatedDate('YYYY-MM-DDT00:00:00Z')
    return {
        "customer-identity-random-value.html": htmlReport(data),
        stdout: textSummary(data, {indent: " ", enableColors: true}),
    };


}


