import http from 'k6/http';
import {baseUrlService} from '../../../resources/services/general.js';
import {body,headers,url}  from '../../../resources/services/customer-identity-random-value/index.js';
import * as tools from '../../../tools/index.js';

export let execute = (params) => {

    let {access_token,c_document_id,c_country,c_commerce,c_channel,c_username} = params;

    headers['Authorization']= `Bearer ${access_token}`;
    headers['X-Date-Time'] = tools.getFormatedDate('YYYY-MM-DDT00:00:00Z');
    headers['X-Document-Id']= c_document_id;
    headers['X-Country']= c_country;
    headers['X-Commerce']= c_commerce;
    headers['X-Channel']= c_channel;

    body.user.username = c_username;


    let response = http.post(`${baseUrlService}/${url}`,JSON.stringify(body) , {headers});

    
    return response;
}