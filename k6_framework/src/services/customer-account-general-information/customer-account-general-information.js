import http from 'k6/http';
import {baseUrlService} from '../../../resources/services/general.js'
import {headers,url}  from '../../../resources/services/customer-account-general-information/index.js'

export let execute = (data) => {

    let {access_token,c_username,c_document_id,c_country,c_commerce,c_channel,c_secretUser} = data;

    headers['Authorization']= `Bearer ${access_token}`;
    headers['X-Customer-Id'] = c_username;
    headers['X-Document-Id']= c_document_id;
    headers['X-Country']= c_country;
    headers['X-Commerce']= c_commerce;
    headers['X-Channel']= c_channel;

    let URL= `${baseUrlService}/${url}/${c_secretUser}`;
    let params = {
        headers
    }


    let response = http.get(URL,params);

    return response;
}