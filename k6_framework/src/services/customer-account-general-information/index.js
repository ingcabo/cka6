
import { check, group, sleep, fail } from 'k6';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import {textSummary} from "https://jslib.k6.io/k6-summary/0.0.1/index.js";
import * as oauthToken from '../oauth-token/oauthToken.js'
import * as cAccountGI_params from '../../../resources/services/customer-account-general-information/index.js'
import * as cAccountGI from './customer-account-general-information.js'
import * as tools from '../../../tools/index.js';

export let options = cAccountGI_params.parametrization_test[__ENV.TYPE_TEST]
const userData = JSON.parse(open(options.filename));

export function setup() {

  let {body} = oauthToken.execute();
  let {access_token} = JSON.parse(body);

  userData.forEach(element => {
    element['access_token'] = access_token;
    element['c_secretUser'] = tools.stringToSha256(element.c_username+access_token);
  });


  return  userData;
}


export default (userData) => {



  let count = tools.metramd(userData);
  let data = userData[count];
  const {c_expectedStatusCode } = data

  let res = cAccountGI.execute(data);


  if (res.status != c_expectedStatusCode){
    tools.entryLog(res);
  }

  check(res, { 
    'response code was ': (res) => res.status == c_expectedStatusCode,
   // 'body contain data': (res) => JSON.parse(res.body).customer.length != '',
  })
  sleep(1)
}

export function handleSummary(data) {
  
  return {
    "customer-account-general-information.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };


}


