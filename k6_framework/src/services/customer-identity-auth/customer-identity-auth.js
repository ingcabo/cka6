import http from 'k6/http';
import {baseUrlService} from '../../../resources/services/general.js'
import {body,headers,url}  from '../../../resources/services/customer-identity-auth/index.js'
import * as tools from '../../../tools/index.js';

export let execute = (params) => {

    const {access_token,c_transaction_type,c_country,c_commerce,c_channel,c_username,c_password,c_secretUser,c_usernameType}=params;
    headers['Authorization']= `Bearer ${access_token}`;
    headers['X-Date-Time'] = tools.getFormatedDate('YYYY-MM-DDT00:00:00Z');
    headers['X-Transaction-Type']= c_transaction_type;
    headers['X-Country']= c_country;
    headers['X-Commerce']= c_commerce;
    headers['X-Channel']= c_channel;
    headers['X-Customer-Id']= c_username;

    body.authentication.user.username = c_username;
    body.authentication.user.usernameType=c_usernameType;
    body.authentication.password = c_password;

    const URL = `${baseUrlService}/${url}/${c_secretUser}/authentication`;

    let response = http.post(URL, JSON.stringify(body), {headers});
    return response;
}