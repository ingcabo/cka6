//report
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import {textSummary} from "https://jslib.k6.io/k6-summary/0.0.1/index.js";
import * as oauthToken from '../oauth-token/oauthToken.js'
import * as cIdentityAuth_params from '../../../resources/services/customer-identity-auth/index.js'
import * as cIdentityAuth from './customer-identity-auth.js'
import * as tools from '../../../tools/index.js';
import { check, group, sleep, fail } from 'k6';


export let options = cIdentityAuth_params.parametrization_test[__ENV.TYPE_TEST]

const userData = JSON.parse(open(options.filename));

export function setup() {

  let {body} = oauthToken.execute();
  let {access_token} = JSON.parse(body);

  userData.forEach(element => {
    element['access_token'] = access_token;
    element['c_secretUser'] = tools.stringToSha256(element.c_username+access_token);
  });

  return userData;
}


export default (userData) => {
  let count = tools.metramd(userData);
  let data = userData[count];
  const {c_expectedStatusCode} = data;
  let res = cIdentityAuth.execute(data)
  if (res.status != c_expectedStatusCode){
    tools.entryLog(res);
  }

  check(res, { 
    'response code was expected ': (res) => res.status == c_expectedStatusCode
    //'body contain data :code': (res) => JSON.parse(res.body).code == data.c_expectedCode_body,
    //'body contain data : message': (res) => JSON.parse(res.body).message == data.c_expectedMessage_body,
   // 'token_type  equal to  Bearer': (res) => JSON.parse(res.body).token_type = "Bearer"
  })
  sleep(1)
}

export function handleSummary(data) {
  return {
    "customer-identity-auth.html" : htmlReport(data),
   stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}


